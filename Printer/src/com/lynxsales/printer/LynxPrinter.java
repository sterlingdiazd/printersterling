package com.lynxsales.printer;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;

import com.lynxsales.printer.entities.Charge;
import com.lynxsales.printer.entities.ClientEntity;
import com.lynxsales.printer.entities.Pedido;
import com.lynxsales.printer.entities.ProductEntity;
import com.lynxsales.printer.entities.UserEntity;
import com.zebra.android.devdemo.util.Utility;

public class LynxPrinter {
	
	private Activity activity;
	private boolean isPriceEnabled;
	
	public LynxPrinter(Activity activity, boolean isPriceEnabled) {
		super();
		this.activity = activity;
		this.isPriceEnabled = isPriceEnabled;
		checkBluetooth();
	}

	private void checkBluetooth() {
		
		
	}

	public void printBill(boolean isForSanut, boolean isTheCopy, String idClient, String docType, String salesOrg, String channel, String sector, String interlocutor, String sellerID, String sellerName, 
			String sellerCel, String companyAddress, String companyRNC, String tel, ArrayList< ArrayList<String> > allProducts)
	{
		ArrayList<Charge> charges = new ArrayList<Charge>();
		
		for(int x = 0; x < allProducts.size(); x++)
		{
			ArrayList<String> product = allProducts.get(x);

			Utility utility = new Utility();
			String date = utility.getDateTime();
			
			Pedido pedido = new Pedido(idClient, date, docType, salesOrg,channel, sector, interlocutor, product.get(0), product.get(1), product.get(2), 
					product.get(3), product.get(4), product.get(5), sellerID, sellerName, sellerCel, companyAddress, companyRNC, tel);
			
			Charge charge = new Charge();
			charge.setPedido(pedido);
			charges.add(charge);
		}
		
		AsyncTaskDiscover asyncTaskDiscover = new AsyncTaskDiscover(activity, charges, isPriceEnabled, isTheCopy, isForSanut);
		asyncTaskDiscover.execute();
		
	}
	
}
