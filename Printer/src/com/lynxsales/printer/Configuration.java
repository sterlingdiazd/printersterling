package com.lynxsales.printer;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Sterling on 11/20/13.
 */
public class Configuration {

    private static Configuration configuration = null;
    private SharedPreferences prefs;
    private SharedPreferences.Editor prefsEditor;
    public static String PREFERENCES;
    public static String MAC_ADDRESS_FRIENDLY_NAME;
    public static String MAC_ADDRESS;
    public static String MANUAL_SEARCH;
    public static boolean choose_id_created_clients;
    
    public Configuration() {
        PREFERENCES = "PREFERENCES";
        MAC_ADDRESS = "MAC_ADDRESS";
        MANUAL_SEARCH = "MANUAL_SEARCH";
    }

    public static synchronized Configuration getInstance() {
        if (configuration == null) {
            configuration = new Configuration();
        }
        return configuration;
    }

    public SharedPreferences getPrefs() {
        return prefs;
    }

    public void setPrefs(SharedPreferences prefs) {
        this.prefs = prefs;
    }

    public SharedPreferences.Editor getPrefsEditor() {
        return prefsEditor;
    }

    public void setPrefsEditor(SharedPreferences.Editor prefsEditor) {
        this.prefsEditor = prefsEditor;
    }

    public void configureSharePreference(Context context){
        this.setPrefs(context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE));
        this.setPrefsEditor(this.getPrefs().edit());
    }

}
