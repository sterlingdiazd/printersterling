package com.lynxsales.printer;

import java.util.ArrayList;

import android.app.Activity;

import com.lynxsales.printer.entities.Charge;
import com.lynxsales.printer.entities.LabelDocument;
import com.lynxsales.printer.entities.LabelElement;
import com.lynxsales.printer.entities.Pedido;

public class DocumentDetail {

	private ArrayList<String> labels;
	private int leftAlignmentLabelKey;
	private int leftAlignmentLabelValue;
	private int labelHeight;
	private int PAGE_WIDTH = 560;
	private double total;
	private Activity activity;
	
	public int getPAGE_WIDTH() {
		return PAGE_WIDTH;
	}

	public void setPAGE_WIDTH(int pAGE_WIDTH) {
		PAGE_WIDTH = pAGE_WIDTH;
	}

	private int labelTop;
	private int column_width;
	private int fontWidth;
	private int fontHeight;
	private ArrayList< ArrayList<String> > alldetails;
	
	private static int DETAIL_LEFT_ALIGNMENT = 20;
	private static int DETAIL_RIGHT_ALIGNMENT = 20;

	public DocumentDetail(Activity activity) {
		this.activity = activity;
		labels = new ArrayList<String>();
		alldetails = new ArrayList< ArrayList<String> >();
	}

	public void addLabel(String labelName) {
		labels.add(labelName);
	}

	public void setLabelDefaults() 
	{	
		leftAlignmentLabelKey = DETAIL_LEFT_ALIGNMENT;
		fontWidth = LabelDocument.FONT_WIDTH;
		fontHeight = LabelDocument.FONT_HEIGHT;
		int columnQuantity = labels.size();
		column_width = (PAGE_WIDTH - DETAIL_LEFT_ALIGNMENT - DETAIL_RIGHT_ALIGNMENT) / columnQuantity;		
	}
	
	public void addDetailValues(ArrayList<Charge> charges) 
	{
		for(int x = 0; x < charges.size(); x++)
		{
			Charge charge = charges.get(x);
			ArrayList<String> detailValues = getDetailValues(charge);
			alldetails.add(detailValues);
		}
	}

	public void addDetailValuesForPedido(ArrayList<Charge> charges) 
	{
		for(int x = 0; x < charges.size(); x++)
		{
			Pedido pedido = charges.get(x).getPedido();
			ArrayList<String> detailValues = getDetailValuesForPedido(pedido, labels);
			alldetails.add(detailValues);
		}
	}
	
	private ArrayList<String> getDetailValuesForPedido(Pedido pedido, ArrayList<String> labels) 
	{
		ArrayList<String> detailValues = new ArrayList<String>();
	
		for(String label : labels)
		{
			if( label.equalsIgnoreCase(activity.getResources().getString(R.string.product)) )
			{
				int largo = pedido.getProductName().length();
				String descripcionProducto;
				if(largo >= 30)
				{
					descripcionProducto = pedido.getProductName().substring(0, 30);
				} else {
					descripcionProducto = pedido.getProductName();
				}
				detailValues.add(descripcionProducto);
			}
			
			if( label.equalsIgnoreCase(activity.getResources().getString(R.string.price)) )
			{
				detailValues.add(pedido.getPrice());
			}
			if( label.equalsIgnoreCase(activity.getResources().getString(R.string.quantity)) )
			{
				detailValues.add(pedido.getQuantity());
			}
			if( label.equalsIgnoreCase(activity.getResources().getString(R.string.amount)) )
			{
				double price = Double.valueOf(pedido.getPrice());
				int quantity =  Integer.valueOf( pedido.getAmount() );
				double amount = price * quantity;
				total += amount;
				
				detailValues.add( amount + "" );
			}
		}
		
		return detailValues;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	private ArrayList<String> getDetailValues(Charge charge) 
	{
		ArrayList<String> detailValues = new ArrayList<String>();

		detailValues.add(charge.getInvoice());
		detailValues.add(charge.getBank());
		detailValues.add(charge.getCheck_number());
		detailValues.add(charge.getAmount());

		return detailValues;
	}

	public ArrayList<String> getLabels() {
		return labels;
	}

	public void setLabels(ArrayList<String> labels) {
		this.labels = labels;
	}

	public int getLeftAlignmentLabelKey() {
		return leftAlignmentLabelKey;
	}

	public void setLeftAlignmentLabelKey(int leftAlignmentLabelKey) {
		this.leftAlignmentLabelKey = leftAlignmentLabelKey;
	}

	public int getLeftAlignmentLabelValue() {
		return leftAlignmentLabelValue;
	}

	public void setLeftAlignmentLabelValue(int leftAlignmentLabelValue) {
		this.leftAlignmentLabelValue = leftAlignmentLabelValue;
	}

	public int getLabelHeight() {
		return labelHeight;
	}

	public void setLabelHeight(int labelHeight) {
		this.labelHeight = labelHeight;
	}

	public int getFontWidth() {
		return fontWidth;
	}

	public void setFontWidth(int fontWidth) {
		this.fontWidth = fontWidth;
	}

	public int getFontHeight() {
		return fontHeight;
	}

	public void setFontHeight(int fontHeight) {
		this.fontHeight = fontHeight;
	}

	public ArrayList<ArrayList<String>> getAlldetails() {
		return alldetails;
	}

	public void setAlldetails(ArrayList<ArrayList<String>> alldetails) {
		this.alldetails = alldetails;
	}
	
	public int getColumn_width() {
		return column_width;
	}

	public void setColumn_width(int column_width) {
		this.column_width = column_width;
	}

	public int getLabelTop() {
		return labelTop;
	}

	public void setLabelTop(int labelTop) {
		this.labelTop = labelTop;
	}

	public void setLabelDefaultsForPedidos() {
		leftAlignmentLabelKey = DETAIL_LEFT_ALIGNMENT;
		fontWidth = LabelDocument.FONT_WIDTH;
		fontHeight = LabelDocument.FONT_HEIGHT;
		int columnQuantity = (labels.size() - 1);
		column_width = (PAGE_WIDTH - DETAIL_LEFT_ALIGNMENT - DETAIL_RIGHT_ALIGNMENT) / columnQuantity;		
		
	}

}
