package com.lynxsales.printer;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Looper;
import android.widget.Toast;
import com.lynxsales.printer.entities.Charge;
import com.lynxsales.printer.entities.DocumentHeader;
import com.lynxsales.printer.entities.LabelDocument;
import com.lynxsales.printer.entities.Pedido;
import com.zebra.android.comm.BluetoothPrinterConnection;
import com.zebra.android.comm.ZebraPrinterConnection;
import com.zebra.android.comm.ZebraPrinterConnectionException;
import com.zebra.android.devdemo.util.DemoSleeper;
import com.zebra.android.discovery.BluetoothDiscoverer;
import com.zebra.android.discovery.DiscoveredPrinter;
import com.zebra.android.discovery.DiscoveredPrinterBluetooth;
import com.zebra.android.discovery.DiscoveryHandler;
import com.zebra.android.printer.PrinterLanguage;
import com.zebra.android.printer.ZebraPrinter;
import com.zebra.android.printer.ZebraPrinterFactory;
import com.zebra.android.printer.ZebraPrinterLanguageUnknownException;

public class AsyncTaskDiscover extends AsyncTask<String,Integer,Boolean> {

    //public ProgressDialog progressDialog;
	private Configuration configuration;
	private ArrayList<Charge> charges;
	private static DiscoveryHandler btDiscoveryHandler = null;
	private ArrayList<String> discoveredPrinters = null;
	private DiscoveredPrinterBluetooth discoveredPrinter;
	private ZebraPrinter printer;
	private ZebraPrinterConnection zebraPrinterConnection;
	private Activity activity;
	private PrinterLanguage printerLanguage;
	private String setPrinterLangueToZPL = "! U1 setvar 'device.languages' 'zpl' ";
	public static String message;
	private boolean isPriceEnabled;
	private boolean wasPrinted;
	private boolean isTheCopy;
	private boolean isForSanut;
	
	public AsyncTaskDiscover(Activity activity, ArrayList<Charge> charges, boolean isPriceEnabled, boolean isTheCopy, boolean isForSanut) {
		super();
		this.activity = activity;
		this.charges = charges;
		this.isPriceEnabled = isPriceEnabled;
		this.isTheCopy = isTheCopy;
		this.isForSanut = isForSanut;
		configuration = Configuration.getInstance();
		configuration.configureSharePreference(activity);
		prepareHandler(activity);
	}

	
	@Override
    protected void onPreExecute() {
        super.onPreExecute();
        /*
        progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Enviando");
        progressDialog.show();
        */
    }

    protected Boolean doInBackground(String... params) {
    	
    	
		String MAC_address = "";
		
		
        boolean printed = false;
        try {			
			
			for(int x = 0; x < 10; x++)
			{
				MAC_address = configuration.getPrefs().getString(Configuration.MAC_ADDRESS, "");
				
				if(MAC_address != null && MAC_address != "")
				{
					//progressDialog.setMessage("Printer Found");
					//Toast.makeText(activity, "Printer Found", Toast.LENGTH_SHORT).show();
					//PrinterConnection pc = new PrinterConnection(activity, charges);
					try {
						//if(pc != null){
							//Looper.prepare();

							printer = connect();
							if (printer != null) {
									
								printed = sendTestLabel();
							} else {
								disconnect();
							}	
							
							//Looper.myLooper().quit();
						//} 
					} catch (Exception e) {
						e.printStackTrace();
						Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
					}
					break;
				}
				else
				{
					BluetoothDiscoverer.findPrinters(activity, btDiscoveryHandler);
					
					Thread.sleep(10000);
					
					
				}
			}
			
			
		} catch (ZebraPrinterConnectionException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
			if(e.getMessage() == null && message != null)
			{
				Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(activity, e.getMessage(), Toast.LENGTH_LONG).show();
			}
			
		}
        
        return printed;
    }
    
    

	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	protected void onPostExecute(Boolean result) 
    {
		wasPrinted = result;
		if(message != null)
		Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
        //progressDialog.dismiss(); 
        
      //  Toast.makeText(context, sapResponse, Toast.LENGTH_SHORT).show();
    }
	
	private void prepareHandler(final Context context) 
	{
		btDiscoveryHandler = new DiscoveryHandler() 
		{
			public void discoveryError(String message) {
				AsyncTaskDiscover.message = message;
			}

			@Override
			public void discoveryFinished() {
				
			}

			@Override
			public void foundPrinter(DiscoveredPrinter printer) 
			{
				discoveredPrinter = (DiscoveredPrinterBluetooth) printer;
				
				String friendlyName = discoveredPrinter.friendlyName;
				String MacAddress = discoveredPrinter.address;
				if(friendlyName != null && friendlyName.contains("XXXX"))
				{
					configuration.getPrefsEditor().putString(Configuration.MAC_ADDRESS_FRIENDLY_NAME, friendlyName);
					configuration.getPrefsEditor().putString(Configuration.MAC_ADDRESS, MacAddress);
					configuration.getPrefsEditor().commit();					
				} else {
					Toast.makeText(context, "Another Device has been discovered. Name: " + friendlyName, Toast.LENGTH_LONG).show();
				}
			}
		};
	}
	
	private ZebraPrinter connect() {
		// setStatus("ConnectING", Color.GREEN);
		zebraPrinterConnection = null;
		String MAC_address = configuration.getPrefs().getString(Configuration.MAC_ADDRESS, "");
		zebraPrinterConnection = new BluetoothPrinterConnection(MAC_address);

		try {
			zebraPrinterConnection.open();
			//progressDialog.setMessage("Printer Connected");
			// setStatus("Connected", Color.GREEN);
		} catch (ZebraPrinterConnectionException e) {
			// setStatus("Comm Error! Disconnecting", Color.RED);
			e.printStackTrace();
			DemoSleeper.sleep(1000);
			disconnect();
		}

		ZebraPrinter printer = null;

		if (zebraPrinterConnection.isConnected()) 
		{
			try 
			{
				printer = ZebraPrinterFactory.getInstance(zebraPrinterConnection);
				printerLanguage = printer.getPrinterControlLanguage();
			} 
			catch (ZebraPrinterConnectionException e) 
			{
				printer = null;
				DemoSleeper.sleep(1000);
				disconnect();
			} 
			catch (ZebraPrinterLanguageUnknownException e) 
			{
				printer = null;
				DemoSleeper.sleep(1000);
				disconnect();
			}
		}

		return printer;
	}
	
	private void disconnect() {
		try {
			// setStatus("Disconnecting", Color.RED);
			if (zebraPrinterConnection != null) {
				zebraPrinterConnection.close();
				//progressDialog.setMessage("Printer Disconnected");
			}
			// setStatus("Not Connected", Color.RED);
		} catch (ZebraPrinterConnectionException e) {
			// setStatus("COMM Error! Disconnected", Color.RED);
		} finally {

		}
	}

	private boolean sendTestLabel() {
		try {

			byte[] configLabel;

			//progressDialog.setMessage("Printing");
			if (charges.get(0).getIdCharge() == null && charges.get(0).getPedido()!= null) 
			{
				configLabel = configBillLaber(charges, isTheCopy, isForSanut);
			} 
			else 
			{
				configLabel = getConfigLabel(charges, isTheCopy);
			}

			zebraPrinterConnection.write(configLabel);
			// setStatus("Sending Data", Color.BLUE);
			DemoSleeper.sleep(1500);
			if (zebraPrinterConnection instanceof BluetoothPrinterConnection) 
			{
				// String friendlyName = ((BluetoothPrinterConnection)
				// zebraPrinterConnection).getFriendlyName();
				// setStatus(friendlyName, Color.MAGENTA);
				DemoSleeper.sleep(500);
			}
		} catch (ZebraPrinterConnectionException e) {
			e.printStackTrace();
			new AlertDialogManager().showAlertDialog(activity, "Printer Error", e.getMessage(), false);
		} finally {
			disconnect();
		}
		
		return true;
	}
	
	private byte[] configBillLaber(ArrayList<Charge> charges, boolean isTheCopy, boolean isForSanut) {
		
		byte[] configLabel = null;
		if (printerLanguage == PrinterLanguage.ZPL) {

			LabelDocument labelDocument = new LabelDocument(activity);

			Charge charge = charges.get(0); // All charges have the same header info
			Pedido pedido = charge.getPedido();
			labelDocument.setPedido(pedido);
			
			labelDocument.setDocumentHeader(labelDocument.createHeaderWithDefaults(activity));
			DocumentHeader documentHeader = labelDocument.getDocumentHeader();
			documentHeader.addHeaderElement(getString(R.string.date), pedido.getDate());
			documentHeader.addHeaderElement(getString(R.string.client), pedido.getSalesOrg());

			labelDocument.setDocumentDetail(new DocumentDetail(activity));
			DocumentDetail documentDetail = labelDocument.getDocumentDetail();

			documentDetail.addLabel(getString(R.string.product));
			
			documentDetail.addLabel(getString(R.string.quantity));
			
			if(isPriceEnabled)
			{
				documentDetail.addLabel(getString(R.string.price));
				documentDetail.addLabel(getString(R.string.amount));
			}
				
			documentDetail.setLabelDefaults();

			documentDetail.addDetailValuesForPedido(charges);

			String label = labelDocument.generateDocument(printer, isPriceEnabled, isTheCopy, isForSanut);
			configLabel = label.getBytes();

		} else if (printerLanguage == PrinterLanguage.CPCL) {
			String cpclConfigLabel = "! 0 200 200 406 1\r\n" +
					""
					+ "ON-FEED IGNORE\r\n" + "BOX 20 20 380 380 8\r\n"
					+ "T 0 6 137 177 TEST\r\n" + "PRINT\r\n";
			configLabel = cpclConfigLabel.getBytes();
		}
		return configLabel;
	}

	private byte[] getConfigLabel(ArrayList<Charge> charges, boolean isTheCopy) {

		byte[] configLabel = null;
		if (printerLanguage == PrinterLanguage.ZPL) {

			LabelDocument labelDocument = new LabelDocument(activity);

			Charge charge = charges.get(0); // All charges have the same header
											// info
			labelDocument.setDocumentHeader(labelDocument
					.createHeaderWithDefaults(activity));
			DocumentHeader documentHeader = labelDocument.getDocumentHeader();

			documentHeader.addHeaderElement(Charge.ID, charge.getIdCharge());
			documentHeader.addHeaderElement(Charge.DATE, charge.getDate());
			documentHeader
					.addHeaderElement(Charge.SOCIETY, charge.getSociety());
			documentHeader.addHeaderElement(Charge.SELLER, charge.getSeller());
			documentHeader.addHeaderElement(Charge.CLIENT, charge.getClient());
			documentHeader.addHeaderElement(Charge.CASH_REGISTER,
					charge.getCashRegister());

			labelDocument.setDocumentDetail(new DocumentDetail(activity));
			DocumentDetail documentDetail = labelDocument.getDocumentDetail();

			documentDetail.addLabel(Charge.INVOICE);
			documentDetail.addLabel(Charge.BANK);
			documentDetail.addLabel(Charge.CHECK);
			documentDetail.addLabel(Charge.AMOUNT);
			documentDetail.setLabelDefaults();

			documentDetail.addDetailValues(charges);

			String label = labelDocument.generateDocument(printer, isPriceEnabled, isTheCopy, isForSanut);
			configLabel = label.getBytes();

		} else if (printerLanguage == PrinterLanguage.CPCL) {
			String cpclConfigLabel = "! 0 200 200 406 1\r\n"
					+ "ON-FEED IGNORE\r\n" + "BOX 20 20 380 380 8\r\n"
					+ "T 0 6 137 177 TEST\r\n" + "PRINT\r\n";
			configLabel = cpclConfigLabel.getBytes();
		}
		return configLabel;
	}

	private String getString(int id)
	{
		return activity.getResources().getString(id);
	}
}