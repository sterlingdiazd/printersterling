package com.lynxsales.printer.entities;

import java.util.ArrayList;

import android.app.Activity;

public class DocumentHeader {

	private ArrayList<LabelElement> header;
	private int leftAlignmentLabelKey;
	private int leftAlignmentLabelValue;
	private int labelTop;
	private int labelHeight;
	private int fontWidth;
	private int fontHeight;
	private Activity activity;

	public DocumentHeader(Activity activity, int leftAlignmentLabelKey, int leftAlignmentLabelValue,
			int labelHeight, int labelTop, int fontWidth, int fontHeight) {
		super();
		header = new ArrayList<LabelElement>();
		this.leftAlignmentLabelKey = leftAlignmentLabelKey;
		this.leftAlignmentLabelValue = leftAlignmentLabelValue;
		this.labelHeight = labelHeight;
		this.labelTop = labelTop;
		this.fontWidth = fontWidth;
		this.fontHeight = fontHeight;
	}

	public DocumentHeader() {
		super();
	}


	public void addHeaderElement(String key, String value) {
		header.add( new LabelElement(key, value) );
	}

	public ArrayList<LabelElement> getHeader() {
		return header;
	}

	public void setHeader(ArrayList<LabelElement> header) {
		this.header = header;
	}

	public int getLeftAlignmentLabelKey() {
		return leftAlignmentLabelKey;
	}

	public void setLeftAlignmentLabelKey(int leftAlignmentLabelKey) {
		this.leftAlignmentLabelKey = leftAlignmentLabelKey;
	}

	public int getLeftAlignmentLabelValue() {
		return leftAlignmentLabelValue;
	}

	public void setLeftAlignmentLabelValue(int leftAlignmentLabelValue) {
		this.leftAlignmentLabelValue = leftAlignmentLabelValue;
	}

	public int getLabelHeight() {
		return labelHeight;
	}

	public void setLabelHeight(int labelHeight) {
		this.labelHeight = labelHeight;
	}

	public int getFontWidth() {
		return fontWidth;
	}

	public void setFontWidth(int fontWidth) {
		this.fontWidth = fontWidth;
	}

	public int getFontHeight() {
		return fontHeight;
	}

	public void setFontHeight(int fontHeight) {
		this.fontHeight = fontHeight;
	}

	public int getLabelTop() {
		return labelTop;
	}

	public void setLabelTop(int labelTop) {
		this.labelTop = labelTop;
	}


}
