package com.lynxsales.printer.entities;

public class Order {

	private String billNumber;
	private String date;
	private String client;
	
	private String labelProduct;
	private String labelPrice;
	private String labelQuantity;
	private String labelWeight;

	private String labelSign;
	private String labelTotal;
	private String valueProduct;
	private String valuePrice;
	private String valueQuantity;
	private String valueWeight;
	private String valueTotal;
	
	public Order() {
		super();
	}

	public Order(String billNumber, String date, String client,
			String labelProduct, String labelPrice, String labelQuantity,
			String labelWeight, String labelSign, String labelTotal,
			String valueProduct, String valuePrice, String valueQuantity,
			String valueWeight, String valueTotal) {
		super();
		this.billNumber = billNumber;
		this.date = date;
		this.client = client;
		this.labelProduct = labelProduct;
		this.labelPrice = labelPrice;
		this.labelQuantity = labelQuantity;
		this.labelWeight = labelWeight;
		this.labelSign = labelSign;
		this.labelTotal = labelTotal;
		this.valueProduct = valueProduct;
		this.valuePrice = valuePrice;
		this.valueQuantity = valueQuantity;
		this.valueWeight = valueWeight;
		this.valueTotal = valueTotal;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getLabelProduct() {
		return labelProduct;
	}

	public void setLabelProduct(String labelProduct) {
		this.labelProduct = labelProduct;
	}

	public String getLabelPrice() {
		return labelPrice;
	}

	public void setLabelPrice(String labelPrice) {
		this.labelPrice = labelPrice;
	}

	public String getLabelQuantity() {
		return labelQuantity;
	}

	public void setLabelQuantity(String labelQuantity) {
		this.labelQuantity = labelQuantity;
	}

	public String getLabelWeight() {
		return labelWeight;
	}

	public void setLabelWeight(String labelWeight) {
		this.labelWeight = labelWeight;
	}

	public String getLabelSign() {
		return labelSign;
	}

	public void setLabelSign(String labelSign) {
		this.labelSign = labelSign;
	}

	public String getLabelTotal() {
		return labelTotal;
	}

	public void setLabelTotal(String labelTotal) {
		this.labelTotal = labelTotal;
	}

	public String getValueProduct() {
		return valueProduct;
	}

	public void setValueProduct(String valueProduct) {
		this.valueProduct = valueProduct;
	}

	public String getValuePrice() {
		return valuePrice;
	}

	public void setValuePrice(String valuePrice) {
		this.valuePrice = valuePrice;
	}

	public String getValueQuantity() {
		return valueQuantity;
	}

	public void setValueQuantity(String valueQuantity) {
		this.valueQuantity = valueQuantity;
	}

	public String getValueWeight() {
		return valueWeight;
	}

	public void setValueWeight(String valueWeight) {
		this.valueWeight = valueWeight;
	}

	public String getValueTotal() {
		return valueTotal;
	}

	public void setValueTotal(String valueTotal) {
		this.valueTotal = valueTotal;
	}
	
	
	
	
	
}
