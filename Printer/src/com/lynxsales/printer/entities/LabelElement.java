package com.lynxsales.printer.entities;

public class LabelElement {
	
	private String labelKey;
	private String labelValue;

	
	public LabelElement(String labelKey, String labelValue) {
		super();
		this.labelKey = labelKey;
		this.labelValue = labelValue;
	}


	public String getLabelKey() {
		return labelKey;
	}
	public void setLabelKey(String labelKey) {
		this.labelKey = labelKey;
	}
	public String getLabelValue() {
		return labelValue;
	}
	public void setLabelValue(String labelValue) {
		this.labelValue = labelValue;
	}
	
	

}
