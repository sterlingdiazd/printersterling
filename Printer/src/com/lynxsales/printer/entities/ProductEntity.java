package com.lynxsales.printer.entities;

import java.io.Serializable;
import java.util.List;


@SuppressWarnings("serial")
public class ProductEntity implements Serializable{

	private String id;
	private String name;
	private String thumb;
	private String salesOrganization;
	private String distributionChannel;
	private String price;
	private String salesUnit;
	private String quantityInSalesUnit;
	private String unitBase;
	private String stock;
	private String credit;
	private String offer;
	private String center;
	private String amount;
	private boolean amountVisible;
	
	private List< String > centers;
	private List< String > units;
	
	
	public ProductEntity() {
	}
	
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}


	public String getThumb() {
		return thumb;
	}


	public void setThumb(String thumb) {
		this.thumb = thumb;
	}


	public String getSalesOrganization() {
		return salesOrganization;
	}


	public void setSalesOrganization(String salesOrganization) {
		this.salesOrganization = salesOrganization;
	}


	public String getDistributionChannel() {
		return distributionChannel;
	}


	public void setDistributionChannel(String distributionChannel) {
		this.distributionChannel = distributionChannel;
	}


	public String getPrice() {
		return price;
	}


	public void setPrice(String price) {
		this.price = price;
	}


	public String getSalesUnit() {
		return salesUnit;
	}


	public void setSalesUnit(String salesUnit) {
		this.salesUnit = salesUnit;
	}


	public String getQuantityInSalesUnit() {
		return quantityInSalesUnit;
	}


	public void setQuantityInSalesUnit(String quantityInSalesUnit) {
		this.quantityInSalesUnit = quantityInSalesUnit;
	}


	public String getUnitBase() {
		return unitBase;
	}


	public void setUnitBase(String unitBase) {
		this.unitBase = unitBase;
	}


	public String getStock() {
		return stock;
	}


	public void setStock(String stock) {
		this.stock = stock;
	}


	public String getCredit() {
		return credit;
	}


	public void setCredit(String credit) {
		this.credit = credit;
	}


	public String getOffer() {
		return offer;
	}


	public void setOffer(String offer) {
		this.offer = offer;
	}


	public List<String> getCenters() {
		return centers;
	}


	public void setCenters(List<String> centers) {
		this.centers = centers;
	}

	public List<String> getUnits() {
		return units;
	}

	public void setUnits(List<String> units) {
		this.units = units;
	}	
	
	public String getCenter() {
		return center;
	}
	
	public void setCenter(String center) {
		this.center = center;
	}
	
	
	public String getAmount() {
		return amount;
	}
	
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	public boolean isAmountVisible() {
		return amountVisible;
	}
	public void setAmountVisible(boolean amountVisible) {
		this.amountVisible = amountVisible;
	}
	
}
