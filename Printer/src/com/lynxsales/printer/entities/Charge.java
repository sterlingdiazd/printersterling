package com.lynxsales.printer.entities;

import org.json.JSONException;
import org.json.JSONObject;

public class Charge {

	
	private String idCharge;

	private String society;
	private String cashRegister;
	private String invoice;
	private String bank;
	private String date;
	private String check_number;
	
	private String seller;
	private String amount;
	private String client;
	private JSONObject json_charge;
	private Pedido pedido;
	
	public static String ID = "Id";
	public static String SOCIETY = "Society";
	public static String CASH_REGISTER = "Cash Register";
	public static String INVOICE = "Invoice";
	public static String BANK = "Bank";
	public static String DATE = "Date";
	public static String CHECK = "Check";
	
	public static String SELLER = "Seller";
	public static String CLIENT = "Client";
	public static String AMOUNT = "Amount";
	
	public static String PRICE = "Price";
	public static String DOC_TYPE = "DocType";
	public static String SALES_ORG = "SalesOrg";
	public static String PRODUCT = "Product";
	public static String CHANNEL = "Channel";
	public static String SECTOR = "Sector";
	public static String INTERLOCUTOR = "Interlocutor";
	public static String USER = "User";
	public static String CENTER = "Center";
	public static String QUANTITY = "Quantity";
	public static String STORE = "Store";
	
	

	
	public Charge() {
		super();
	}

	public Charge(JSONObject json_charge) 
	{
		if(json_charge != null)
		{
			this.json_charge = json_charge;
			convertJsonToObject();
		}
	}




	private void convertJsonToObject() {
		try {
			this.idCharge = json_charge.getString("id");
			this.amount = json_charge.getString("amount");
			this.client = json_charge.getString("client");
			this.society = json_charge.getString("society");
			this.cashRegister = json_charge.getString("cash_register");
			this.invoice = json_charge.getString("invoice");
			this.bank = json_charge.getString("bank");
			this.date = json_charge.getString("date");
			this.check_number = json_charge.getString("check_no");
			this.seller = json_charge.getString("seller");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}

	
	public Charge(String idCharge, String amount, String client,
			String society, String cashRegister, String invoice, String bank,
			String date, String check_number, String seller) {
		super();
		this.idCharge = idCharge;
		this.amount = amount;
		this.client = client;
		this.society = society;
		this.cashRegister = cashRegister;
		this.invoice = invoice;
		this.bank = bank;
		this.date = date;
		this.check_number = check_number;
		this.seller = seller;
	}

	public String getIdCharge() {
		return idCharge;
	}

	public void setIdCharge(String idCharge) {
		this.idCharge = idCharge;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getSociety() {
		return society;
	}

	public void setSociety(String society) {
		this.society = society;
	}

	public String getCashRegister() {
		return cashRegister;
	}

	public void setCashRegister(String cashRegister) {
		this.cashRegister = cashRegister;
	}

	public String getInvoice() {
		return invoice;
	}

	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCheck_number() {
		return check_number;
	}

	public void setCheck_number(String check_number) {
		this.check_number = check_number;
	}

	public String getSeller() {
		return seller;
	}

	public void setSeller(String seller) {
		this.seller = seller;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	
}
