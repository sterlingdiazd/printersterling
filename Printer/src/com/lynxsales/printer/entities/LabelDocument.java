package com.lynxsales.printer.entities;

import java.util.ArrayList;

import android.app.Activity;

import com.lynxsales.printer.DocumentDetail;
import com.lynxsales.printer.R;
import com.zebra.android.printer.ZebraPrinter;

public class LabelDocument {

	private static final int IMAGE_HEIGHT = 90;
	private DocumentHeader documentHeader;
	private DocumentDetail documentDetail;
	private Pedido pedido;

	public static int HEADER_LEFT_ALIGNMENT_LABEL_KEY = 20;
	public static int HEADER_LEFT_ALIGNMENT_LABEL_VALUE = 200;
	public static int LABEL_HEIGHT = 40;
	public static int LABEL_TOP = 150;
	public static int FONT_WIDTH = 20;
	public static int FONT_HEIGHT = 30;
	
	private String documentStart = "^XA";
	private String documentEnd = "^XZ";
	private String labelDefinition = "^FO";
	private String labelStart = "^FD";
	private String labelLenghtCommand = "^LL";
	private String imageDefinition = "^XGE:";
	private String fileExtension = ".GRF";
	private String labelEnd = "^FS";
	private String font = "^A0N";
	private String comma = ",";
	private String figure = "^GB";

	private String line = ",2,2,B,0";
	private Activity activity;
	private String lineDivider;
	private boolean isPriceEnabled;
	
	public LabelDocument(Activity activity) {
		this.activity = activity;
	}

	public int getCentralizedLeftMargin(String text)
	{
		int word_size = text.trim().length();
		int world_lenght = ( (FONT_WIDTH / 2) * word_size);
		int pageWidth  = getDocumentDetail().getPAGE_WIDTH();
		int left = ((pageWidth - world_lenght ) / 2) ;
		return left;
	}
	
	public DocumentHeader createHeaderWithDefaults(Activity activity) 
	{		
		return new DocumentHeader(activity, HEADER_LEFT_ALIGNMENT_LABEL_KEY, HEADER_LEFT_ALIGNMENT_LABEL_VALUE, LABEL_HEIGHT, LABEL_TOP,FONT_WIDTH, FONT_HEIGHT);
	}

	public DocumentHeader createHeader(Activity activity, int leftAlignmentLabelKey, int leftAlignmentLabelValue, int labelHeight, int labelTop, int fontWidth, int fontHeight) 
	{
		return new DocumentHeader(activity, leftAlignmentLabelKey, leftAlignmentLabelValue, labelHeight, labelTop, fontWidth, fontHeight );
	}
	
	public DocumentHeader getDocumentHeader() {
		return documentHeader;
	}

	public void setDocumentHeader(DocumentHeader documentHeader) {
		this.documentHeader = documentHeader;
	}

	public DocumentDetail getDocumentDetail() {
		return documentDetail;
	}

	public void setDocumentDetail(DocumentDetail documentDetail) {
		this.documentDetail = documentDetail;
	}
	
	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public String generateDocument(ZebraPrinter printer, boolean isPriceEnabled, boolean isTheCopy, boolean isForSanut) 
	{
		String stringCorporative = generateCorporativeSection(isForSanut);
		String stringHeader = generateStringHeader();		
		String stringDetailLabels = generateDetailLabers();
		String stringDetail = generateStringDetail();	
		
		String total = "";
		if(isPriceEnabled)
			total = generateTotal();
		
		String sellerName = generateSellerName(isForSanut);
		String sellerCel = "";
		if(!isForSanut)
		{
			sellerCel = generateSellerCel();	
		}
		String stringSign = generateSign(isTheCopy);
		String labelLenght = labelLenghtCommand + (getDocumentHeader().getLabelTop() + getDocumentHeader().getLabelHeight());
		String label = documentStart + labelLenght + stringCorporative + stringHeader + stringDetailLabels + stringDetail + total + sellerName + sellerCel + stringSign + documentEnd;
		return label;
	}

	private String generateSellerName(boolean isForSanut) 
	{
		int labelTop = getDocumentHeader().getLabelTop() + (getDocumentHeader().getFontHeight() * 2);

		String sellerNameKey = "";
		String sellerNameValue = "";
		String sellerLabel = "Vendedor";
		
		if(isForSanut)
		{
			sellerLabel = "Entregado por";
		}
		sellerNameKey = labelDefinition + getDocumentHeader().getLeftAlignmentLabelKey() + comma + 
					labelTop + font + comma + 
					getDocumentHeader().getFontWidth() + comma + getDocumentHeader().getFontHeight() + 
					labelStart + sellerLabel + labelEnd;
			
		sellerNameValue = labelDefinition + (getDocumentHeader().getLeftAlignmentLabelValue() + getDocumentHeader().getFontWidth() + 10) + comma +
					labelTop + font + comma + 
					getDocumentHeader().getFontWidth() + comma + getDocumentHeader().getFontHeight() + 
					labelStart + pedido.getSellerName() + labelEnd;
		
			this.getDocumentHeader().setLabelTop(labelTop + getDocumentHeader().getLabelHeight());
			
		return sellerNameKey + sellerNameValue;
	}
	
	private String generateSellerCel() 
	{
		int labelTop = getDocumentHeader().getLabelTop();

		String sellerNameKey = "";
		String sellerNameValue = "";

		sellerNameKey = labelDefinition + getDocumentHeader().getLeftAlignmentLabelKey() + comma + 
					labelTop + font + comma + 
					getDocumentHeader().getFontWidth() + comma + getDocumentHeader().getFontHeight() + 
					labelStart + "Celular" + labelEnd;
			
		sellerNameValue = labelDefinition + (getDocumentHeader().getLeftAlignmentLabelValue() + getDocumentHeader().getFontWidth() + 10) + comma +
					labelTop + font + comma + 
					getDocumentHeader().getFontWidth() + comma + getDocumentHeader().getFontHeight() + 
					labelStart + pedido.getSellerCel() + labelEnd;
		
			this.getDocumentHeader().setLabelTop(labelTop + getDocumentHeader().getLabelHeight());
			
		return sellerNameKey + sellerNameValue;
	}

	private String generateTotal() 
	{
		String stringHeader = "";
		int labelTop = getDocumentHeader().getLabelTop() + (getDocumentHeader().getLabelHeight()*2);
		String labelKey = "";
		String labelValue = "";

			labelKey = labelDefinition + getDocumentHeader().getLeftAlignmentLabelKey() + comma + 
					labelTop + font + comma + 
					getDocumentHeader().getFontWidth() + comma + getDocumentHeader().getFontHeight() + 
					labelStart + "Total" + labelEnd;
			
			labelValue = labelDefinition + (getDocumentHeader().getLeftAlignmentLabelValue() * 2) + comma +
					labelTop + font + comma + 
					getDocumentHeader().getFontWidth() + comma + getDocumentHeader().getFontHeight() + 
					labelStart + getDocumentDetail().getTotal() + labelEnd;
			
			stringHeader = labelKey + labelValue;
		
		this.getDocumentHeader().setLabelTop(labelTop + getDocumentHeader().getLabelHeight());
		return stringHeader;
	}
	
	private String generateSign(boolean isTheCopy) 
	{
		int labelTop = getDocumentHeader().getLabelTop() + (getDocumentHeader().getFontHeight() * 3);
		String signFigure = "";
		if(!isTheCopy)
		{
			int lineWidth = (getDocumentDetail().getPAGE_WIDTH() * 8) / 10;
			int left = ( (getDocumentDetail().getPAGE_WIDTH() - lineWidth)/ 2);
			signFigure = labelDefinition + left + comma + labelTop + figure + lineWidth + line + labelEnd;
		}
		
		String signLabelText = "Firma de Recibo";
		if(isTheCopy)
		{
			signLabelText = "Copia del Cliente";
		}
				
		int leftSignText = getCentralizedLeftMargin(signLabelText.trim());
		String signLabel = labelDefinition + leftSignText + comma + (labelTop + getDocumentHeader().getFontHeight()) + font + comma + 
		getDocumentHeader().getFontWidth() + comma + getDocumentHeader().getFontHeight() + labelStart + signLabelText + labelEnd;
		
		this.getDocumentHeader().setLabelTop(labelTop + getDocumentHeader().getLabelHeight());
		
		return signFigure + signLabel;
	}
	
	private String generateHorizontalLine() 
	{
		int labelTop = getDocumentHeader().getLabelTop() + (getDocumentHeader().getFontHeight() * 1);
		
		int lineWidth = (getDocumentDetail().getPAGE_WIDTH() * 10) / 10;
		int left = ( (getDocumentDetail().getPAGE_WIDTH() - lineWidth)/ 2);
		String signFigure = labelDefinition + left + comma + labelTop + figure + lineWidth + line + labelEnd;
	
		this.getDocumentHeader().setLabelTop(labelTop /*+ getDocumentHeader().getLabelHeight()*/);
		
		return signFigure;
	}

	private String generateCorporativeSection(boolean isForSanut) 
	{
		String stringCorporative, stringLogo, stringCompanyName, stringDocumentType;
		String fileName = this.pedido.getClient();
		
		int imageWidth = 160;
		
		int leftImage = (getDocumentDetail().getPAGE_WIDTH()   - imageWidth) / 2;		
		if(isForSanut)
		{
			stringLogo = "";
		} else {
			stringLogo = labelDefinition  + leftImage  + comma +  LABEL_TOP  + imageDefinition + fileName + fileExtension  + labelEnd;
			getDocumentHeader().setLabelTop(LABEL_TOP + IMAGE_HEIGHT);
		}
		
		int leftCompanyName = getCentralizedLeftMargin(fileName);	
		stringCompanyName = labelDefinition  + leftCompanyName  + comma +  getDocumentHeader().getLabelTop() + font + comma + (FONT_WIDTH +20) + comma  + FONT_HEIGHT 
				+ labelStart + fileName + labelEnd;		
		getDocumentHeader().setLabelTop(getDocumentHeader().getLabelTop() + FONT_HEIGHT);
		
		
		int leftRNC = getCentralizedLeftMargin(pedido.getCompanyRNC());
		String stringRNC = labelDefinition  + leftRNC + comma +   getDocumentHeader().getLabelTop() 
				+ font + comma + FONT_WIDTH + comma  + FONT_HEIGHT + labelStart + pedido.getCompanyRNC() + labelEnd;
		getDocumentHeader().setLabelTop(getDocumentHeader().getLabelTop() + FONT_HEIGHT);
		
		int leftAddress = getCentralizedLeftMargin( ( (pedido.getCompanyAddress() != null) ? pedido.getCompanyAddress() : "") );
		String stringAddress = labelDefinition  +  leftAddress + comma +   getDocumentHeader().getLabelTop() 
				+ font + comma + FONT_WIDTH + comma  + FONT_HEIGHT + labelStart + ( (pedido.getCompanyAddress() != null) ? pedido.getCompanyAddress() : "") + labelEnd;
		getDocumentHeader().setLabelTop(getDocumentHeader().getLabelTop() + FONT_HEIGHT);

		int leftTEL = getCentralizedLeftMargin(pedido.getTel());
		String stringTel = labelDefinition  + leftTEL  + comma +   getDocumentHeader().getLabelTop() 
				+ font + comma + FONT_WIDTH + comma  + FONT_HEIGHT + labelStart + pedido.getTel() + labelEnd;
		getDocumentHeader().setLabelTop(getDocumentHeader().getLabelTop() + (FONT_HEIGHT*2));
		
		int leftDocType = getCentralizedLeftMargin("PEDIDO");
		stringDocumentType = labelDefinition  + leftDocType  + comma +   getDocumentHeader().getLabelTop() 
		+ font + comma + FONT_WIDTH + comma  + FONT_HEIGHT + labelStart + "PEDIDO" + labelEnd;
		getDocumentHeader().setLabelTop(getDocumentHeader().getLabelTop() + FONT_HEIGHT);
		
		stringCorporative = /* stringLogo + */ stringCompanyName + stringRNC + stringAddress + stringTel + stringDocumentType;
		getDocumentHeader().setLabelTop(getDocumentHeader().getLabelTop() + (FONT_HEIGHT*2));
		
		return stringCorporative;
	}

	private String generateStringHeader() 
	{
		String stringHeader = "";
		
		ArrayList<LabelElement> header = this.getDocumentHeader().getHeader();
		String labelKey = "";
		String labelValue = "";
		int labelTop = 0;
		
		
		for(int x = 0; x < header.size(); x++)
		{
			labelTop = getDocumentHeader().getLabelTop();
			if(x != 0){
				labelTop = labelTop + getDocumentHeader().getFontHeight();
				getDocumentHeader().setLabelTop(labelTop);
			}
			
			labelKey = labelDefinition + getDocumentHeader().getLeftAlignmentLabelKey() + comma + 
					getDocumentHeader().getLabelTop() + font + comma + 
					getDocumentHeader().getFontWidth() + comma + getDocumentHeader().getFontHeight() + 
					labelStart + header.get(x).getLabelKey() + labelEnd;
			
			labelValue = labelDefinition + getDocumentHeader().getLeftAlignmentLabelValue() + comma +
					getDocumentHeader().getLabelTop() + font + comma + 
					getDocumentHeader().getFontWidth() + comma + getDocumentHeader().getFontHeight() + 
					labelStart + header.get(x).getLabelValue() + labelEnd;
			
			stringHeader += labelKey + labelValue;
			
		}

		return stringHeader;
	}
	
	private String generateDetailLabers() 
	{
		String stringDetailLabels = "";
		String label = "";
		ArrayList<String> labels = this.getDocumentDetail().getLabels();
		int top = getDocumentHeader().getLabelTop();
		int height = getDocumentHeader().getLabelHeight();
		int labelTop = top + (FONT_HEIGHT * 2);
		int leftAlignment = getDocumentDetail().getLeftAlignmentLabelKey();
		
		for(int x = 0; x < labels.size(); x++)
		{
			
			String labelKey = labels.get(x);
			
			if(x == 0)
			{
				
				
			} 
			else if(x == 1)
			{
				if(labels.get(0).equalsIgnoreCase(getString(R.string.product)))
				{
					labelTop += FONT_HEIGHT;
				}
			}
			else {
				leftAlignment = leftAlignment + getDocumentDetail().getColumn_width();
				getDocumentDetail().setLeftAlignmentLabelKey(leftAlignment);
			}
			
			label = labelDefinition +  leftAlignment +  comma + labelTop + 
					font + comma + getDocumentDetail().getFontWidth()+ comma + getDocumentDetail().getFontHeight() + 
					labelStart + labelKey + labelEnd;
			
			//labelTop += height;
			stringDetailLabels += label;
		}
		getDocumentDetail().setLeftAlignmentLabelKey(HEADER_LEFT_ALIGNMENT_LABEL_KEY);
		this.getDocumentHeader().setLabelTop(labelTop);
		return stringDetailLabels +  generateHorizontalLine() ;
	}

	private String generateStringDetail() 
	{
		String stringDetail = "";
		String stringDetails = "";
		ArrayList<ArrayList<String>> allDetails = this.getDocumentDetail().getAlldetails();
		int labelTop = getDocumentHeader().getLabelTop() + getDocumentHeader().getLabelHeight();
		int originalLeftAlignment = getDocumentDetail().getLeftAlignmentLabelKey();
		int leftAlignment = originalLeftAlignment;
		
		for(int x = 0; x < allDetails.size(); x++)
		{
			ArrayList<String> details = allDetails.get(x);
			
			for(int y = 0; y < details.size(); y++)
			{
				
				String stringDetailValue = details.get(y);	
				
				if(y == 0)
				{
					stringDetail = labelDefinition + leftAlignment + comma + labelTop + 
							font + comma + getDocumentHeader().getFontWidth() + comma + getDocumentHeader().getFontHeight() + 
							labelStart + stringDetailValue + labelEnd;					
				} else if(y == 1) 
				{
					stringDetail = labelDefinition + leftAlignment + comma + labelTop + 
							font + comma + getDocumentHeader().getFontWidth() + comma + getDocumentHeader().getFontHeight() + 
							labelStart + stringDetailValue + labelEnd;
					getDocumentDetail().setLeftAlignmentLabelKey(leftAlignment);
					
				} 
				else 
				{
					leftAlignment = leftAlignment + getDocumentDetail().getColumn_width();
					getDocumentDetail().setLeftAlignmentLabelKey(leftAlignment);
					stringDetail = labelDefinition + leftAlignment + comma + labelTop + 
							font + comma + getDocumentHeader().getFontWidth() + comma + getDocumentHeader().getFontHeight() + 
							labelStart + stringDetailValue + labelEnd;
					getDocumentHeader().setLabelTop(labelTop);
				}
				
				labelTop = labelTop + getDocumentHeader().getLabelHeight();
				stringDetails += stringDetail;
			}
			leftAlignment = originalLeftAlignment;
		}
		getDocumentHeader().setLabelTop(labelTop);
		getDocumentDetail().setLeftAlignmentLabelKey(HEADER_LEFT_ALIGNMENT_LABEL_KEY);
		return stringDetails;
	}

	private String getString(int id)
	{
		return activity.getResources().getString(id);
	}
	
}
