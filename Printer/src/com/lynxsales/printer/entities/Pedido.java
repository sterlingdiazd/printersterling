package com.lynxsales.printer.entities;

public class Pedido {
	
	private String sellerID;
	private String sellerName, sellerCel;
	private String companyAddress, companyRNC, tel;
	private String amount;
	private String client;
	private String date;
	
	private String docType;
	private String salesOrg;
	private String channel;
	private String sector;
	private String interlocutor;
	private String articlesQueryString;
	private String center;
	private String productName;
	private String price;
	private String quantity;
	private String productId;

	

	
	
	public Pedido(String client, String date, String docType, String salesOrg, String channel, String sector, String interlocutor, String productId, String productName, 
	String price, String quantity, String amount, String center, String sellerID, String sellerName, String sellerCel, String companyAddress, String companyRNC, String tel)
	{
		this.client = client;
		this.date = date;
		this.docType = docType;
		this.salesOrg = salesOrg;
		this.channel = channel;
		this.sector = sector;
		this.interlocutor = interlocutor;
		this.productId = productId;
		this.productName = productName;
		this.price = price;
		this.quantity = quantity;
		this.amount = amount;
		this.center = center; 
		this.sellerID = sellerID;
		this.sellerName = sellerName;
		this.sellerCel = sellerCel;
		this.companyAddress = companyAddress;
		this.companyRNC = companyRNC;
		this.tel =tel;
	}


	public String getSeller() {
		return sellerID;
	}


	public void setSeller(String seller) {
		this.sellerID = seller;
	}


	public String getAmount() {
		return amount;
	}


	public void setAmount(String amount) {
		this.amount = amount;
	}


	public String getClient() {
		return client;
	}


	public void setClient(String client) {
		this.client = client;
	}


	public String getDocType() {
		return docType;
	}


	public void setDocType(String docType) {
		this.docType = docType;
	}


	public String getSalesOrg() {
		return salesOrg;
	}


	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}


	public String getChannel() {
		return channel;
	}


	public void setChannel(String channel) {
		this.channel = channel;
	}


	public String getSector() {
		return sector;
	}


	public void setSector(String sector) {
		this.sector = sector;
	}


	public String getInterlocutor() {
		return interlocutor;
	}


	public void setInterlocutor(String interlocutor) {
		this.interlocutor = interlocutor;
	}


	public String getArticlesQueryString() {
		return articlesQueryString;
	}


	public void setArticlesQueryString(String articlesQueryString) {
		this.articlesQueryString = articlesQueryString;
	}


	public String getCenter() {
		return center;
	}


	public void setCenter(String center) {
		this.center = center;
	}

	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public String getPrice() {
		return price;
	}


	public void setPrice(String price) {
		this.price = price;
	}


	public String getQuantity() {
		return quantity;
	}


	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}


	public String getProductId() {
		return productId;
	}


	public void setProductId(String productId) {
		this.productId = productId;
	}


	public String getSellerName() {
		return sellerName;
	}


	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}


	public String getSellerCel() {
		return sellerCel;
	}


	public void setSellerCel(String sellerCel) {
		this.sellerCel = sellerCel;
	}


	public String getCompanyAddress() {
		return companyAddress;
	}


	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}


	public String getCompanyRNC() {
		return companyRNC;
	}


	public void setCompanyRNC(String companyRNC) {
		this.companyRNC = companyRNC;
	}


	public String getTel() {
		return tel;
	}


	public void setTel(String tel) {
		this.tel = tel;
	}
	
	
	
}
