package com.lynxsales.printer;

import com.lynxsales.printer.Configuration;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;


public class AlertDialogManager {
	/**
	 * Function to display simple Alert Dialog
	 * @param activity - application context
	 * @param title - alert dialog title
	 * @param message - alert message
	 * @param status - success/failure (used to set icon)
	 * 				 - pass null if you don't want icon
	 * */
	private Configuration configuracion;


    public AlertDialogManager() {
		super();
	}

	public void showAlertDialog(final Activity activity, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);

		if(status != null)

        if(status)
        {
            alertDialog.setIcon(R.drawable.ic_ok);
        } else {
            alertDialog.setIcon(R.drawable.ic_logout);
        }

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}
    

	
   

}
